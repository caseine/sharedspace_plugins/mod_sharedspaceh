<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 * @author  Piers Harding  piers@catalyst.net.nz
 * @license http://www.gnu.org/copyleft/gpl.html GNU Public License, mod/sharedspaceh is a work derived from Moodle mod/resoruce
 * @package sharedspaceh
 *
 */
namespace mod_sharedspaceh;

use \StdClass;
use \moodle_url;

defined('MOODLE_INTERNAL') || die();

/**
 * \mod_sharedspaceh\base is the base class for sharedspaceh types
 *
 * This class provides all the functionality for a sharedspaceh
 */
class base {

    public $cm; // If representing a course module.

    public $sharedspaceh; // The sharedresouce record.

    public $sharedspacehentry; // If representing both course module or single resource entry.

    public $navlinks;

    public $parameters;

    public $maxparameters = 5;

    /**
     * Constructor for the base sharedspaceh class
     *
     * Constructor for the base sharedspaceh class.
     * If cmid is set create the cm, course, sharedspaceh objects.
     * and do some checks to make sure people can be here, and so on.
     *
     * @param cmid         integer, the current course module id - not set for new sharedspacehs
     * @param identifier   hash, alternative direct identifier for a sharedspaceh - not set for new sharedspacehs
     */
    public function __construct($cmid = 0, $identifier = false) {
        global $CFG, $COURSE, $DB, $PAGE, $OUTPUT;

        $this->sharedspaceh = new StdClass();
        $this->sharedspaceh->type = 'file'; // Cannot be anything else.
        $this->sharedspaceh->course = $COURSE->id; // This is a default in case of.
        $this->sharedspaceh->introformat = FORMAT_MOODLE;
        $this->navlinks = array();
        $this->inpopup = false;

        // If course module is given, we should get all information pulled from the course module instance.
        if ($cmid) {
            if (! $this->cm = get_coursemodule_from_id('sharedspaceh', $cmid)) {
                print_error('invalidcoursemodule');
            }
            if (!$this->sharedspaceh = $DB->get_record('sharedspaceh', array('id' => $this->cm->instance))) {
                print_error('invalidsharedspaceh', 'sharedspaceh');
            }
            if (! $this->sharedspacehentry = $DB->get_record('sharedspaceh_entry', array('identifier' => $this->sharedspaceh->identifier))) {
                $returnurl = new moodle_url('/course/view.php', array('id' => $COURSE->id));
                print_error('errorinvalididentifier', 'sharedspaceh', $returnurl, $this->sharedspaceh->identifier);
            }

            $coursename = $DB->get_field('course', 'shortname', array('id' => $this->sharedspaceh->course));

            if (!$this->cm->visible &&
                    !has_capability('moodle/course:viewhiddenactivities', \context_module::instance($this->cm->id))) {
                $pagetitle = strip_tags($coursename.': '.$this->strsharedspaceh);
                $navigation = build_navigation($this->navlinks, $this->cm);
                $coursecontext = \context_course::instance($this->sharedspaceh->course);
                $PAGE->set_pagelayout('standard');
                $PAGE->set_context($coursecontext);
                $url = new moodle_url('/mod/sharedspaceh/view.php');
                $PAGE->set_url($url);
                $PAGE->set_title($SITE->fullname);
                $PAGE->set_heading($SITE->fullname);
                $PAGE->navbar->add('view sharedspaceh info', 'view.php', 'misc');
                $PAGE->set_focuscontrol('');
                $PAGE->set_cacheable(false);
                $PAGE->set_button('');

                echo $OUTPUT->header();

                $returnurl = new moodle_url('/course/view.php', array('id' => $this->sharedspaceh->course));
                echo $OUTPUT->notification(get_string("activityiscurrentlyhidden"), $returnurl);
                echo $OUTPUT->footer();
                die;
            }
        } elseif ($identifier) {
            // This may be a new instance so not course module yet.
            if (! $this->sharedspacehentry = $DB->get_record('sharedspaceh_entry', array('identifier' => $identifier))) {
                print_error('errorinvalididentifier', 'sharedspaceh', new moodle_url('/course/view.php', array('id' => $COURSE->id)), $identifier);
            }
            $this->sharedspaceh->identifier = $identifier;
        } else {
            // Empty sharedspaceh
        }

        if (isset($this->sharedspaceh) && !isset($this->sharedspaceh->intro) && isset($this->sharedspacehentry)) {
            $this->sharedspaceh->intro = $this->sharedspacehentry->description;
        }

        $this->strsharedspaceh  = get_string('modulename', 'sharedspaceh');
        $this->strsharedspacehs = get_string('modulenameplural', 'sharedspaceh');
    }

    /**
     * accessor for setting the display attribute for window popup
     */
    public function inpopup() {
        $this->inpopup = true;
    }

    /**
     * form post process for preparing layout parameters properly
     */
    public function _postprocess() {
        global $METASHAREDRC_WINDOW_OPTIONS;

        $alloptions = $METASHAREDRC_WINDOW_OPTIONS;

        $resource = $this->sharedspaceh;

        if (!empty($resource->forcedownload)) {
            $resource->popup = '';
            $resource->options = 'forcedownload';
        } else if (@$resource->windowpopup) {
            $optionlist = array();
            foreach ($alloptions as $option) {
                if(isset($resource->$option)) {
                    $optionlist[] = $option."=".$resource->$option;
                    unset($resource->$option);
                }
            }
            $resource->popup = implode(',', $optionlist);
            unset($resource->windowpopup);
            $resource->options = '';
        } else {
            if (empty($resource->framepage)) {
                $resource->options = '';
            } else {
                $resource->options = 'frame';
            }
            unset($resource->framepage);
            $resource->popup = '';
        }
        $optionlist = array();
        for ($i = 0; $i < $this->maxparameters; $i++) {
            $parametername = "parameter$i";
            $parsename = "parse$i";
            if (!empty($resource->$parsename) and $resource->$parametername != "-") {
                $optionlist[] = $resource->$parametername."=".$resource->$parsename;
            }
            unset($resource->$parsename);
            unset($resource->$parametername);
        }
        $resource->alltext = implode(',', $optionlist);
    }

    // Magic setter.
    public function __set($field, $value){
        if (in_array($field, array('id', 'course', 'name', 'identifier', 'intro', 'introformat', 'alltext', 'popup', 'options'))){
            $this->sharedspaceh->$field = $value;
        }
    }

    // Magic getter.
    public function __get($field) {
        if (in_array($field, array('id', 'course', 'name', 'identifier', 'intro', 'introformat', 'alltext', 'popup', 'options'))) {
            return $this->sharedspaceh->$field;
        }
    }

    /**
     * Display the file resource
     *
     * Displays a file resource embedded, in a frame, or in a popup.
     * Output depends on type of file resource.
     *
     */
    public function display() {
        global $CFG, $THEME, $USER, $PAGE, $OUTPUT, $SITE, $DB, $FULLME;

        $config = get_config('sharedspaceh');

        // Set up some shorthand variables.
        $cm = $this->cm;
        $course = $DB->get_record('course', array('id' => $this->sharedspaceh->course));
        $resource = $this->sharedspaceh;
        $sharedspacehentry = $this->sharedspacehentry;

        $DB->set_field('sharedspaceh_entry', 'scoreview', $sharedspacehentry->scoreview + 1, array('id' => $sharedspacehentry->id));

        // If we dont get the resource then fail.
        if (!$this->sharedspacehentry) {
            sharedspaceh_not_found($course->id);
        }
        $resource->reference = (!empty($sharedspacehentry->file)) ? $sharedspacehentry->file : $sharedspacehentry->url;

        if (isset($resource->name)) {
            $resource->title = $resource->name;
        } else {
            $resource->title = $sharedspacehentry->title;
        }

        $this->set_parameters(); // Set the parameters array.

        // First, find out what sort of file we are dealing with.
        require_once($CFG->libdir.'/filelib.php');
        $querystring = '';
        $resourcetype = '';
        $embedded = false;
        $mimetype = mimeinfo('type', $resource->reference);
        $pagetitle = strip_tags($course->shortname.': '.format_string($resource->title));
        $formatoptions = new stdClass();
        $formatoptions->noclean = true;

        if ($this->inpopup || (isset($resource->options) && $resource->options != 'forcedownload')) {
            if (in_array($mimetype, array('image/gif','image/jpeg','image/png'))) {
                $resourcetype = 'image';
                $embedded = true;
            } else if ($mimetype == 'audio/mp3') {    // It's an MP3 audio file
                $resourcetype = 'mp3';
                $embedded = true;
            } else if ($mimetype == 'video/x-flv') {    // It's a Flash video file
                $resourcetype = 'flv';
                $embedded = true;
            } else if (substr($mimetype, 0, 10) == 'video/x-ms') {   // It's a Media Player file
                $resourcetype = 'mediaplayer';
                $embedded = true;
            } else if ($mimetype == 'video/quicktime') {   // It's a Quicktime file
                $resourcetype = 'quicktime';
                $embedded = true;
            } else if ($mimetype == 'application/x-shockwave-flash') {   // It's a Flash file
                $resourcetype = 'flash';
                $embedded = true;
            } else if ($mimetype == 'video/mpeg') {   // It's a Mpeg file
                $resourcetype = 'mpeg';
                $embedded = true;
            } else if ($mimetype == 'text/html') {    // It's a web page
                $resourcetype = "html";
            } else if ($mimetype == 'application/zip') {    // It's a zip archive
                $resourcetype = 'zip';
                $embedded = true;
            } else if ($mimetype == 'application/pdf' || $mimetype == 'application/x-pdf') {
                $resourcetype = 'pdf';
                $embedded = true;
            } else if ($mimetype == 'audio/x-pn-realaudio') {   // It's a realmedia file
                $resourcetype = 'rm';
                $embedded = true;
            } 
        }
        $isteamspeak = (stripos($resource->reference, 'teamspeak://') === 0);

        // Form the parse string.
        $querys = array();
        if (!empty($resource->alltext)) {
            $parray = explode(',', $resource->alltext);
            foreach ($parray as $fieldstring) {
                list($moodleparam, $urlname) = explode('=', $fieldstring);
                $value = urlencode($this->parameters[$moodleparam]['value']);
                $querys[urlencode($urlname)] = $value;
                $querysbits[] = urlencode($urlname) . '=' . $value;
            }
            if ($isteamspeak) {
                $querystring = implode('?', $querysbits);
            } else {
                $querystring = implode('&amp;', $querysbits);
            }
        }

        // Set up some variables.
        $inpopup = optional_param('inpopup', 0, PARAM_BOOL);
        if (sharedspaceh_is_url($sharedspacehentry->url)) {

            // Shared resource is a pure URL.
            $fullurl = $sharedspacehentry->url;
            if (!empty($querystring)) {
                $urlpieces = parse_url($sharedspacehentry->url);
                if (empty($urlpieces['query']) or $isteamspeak) {
                    $fullurl .= '?'.$querystring;
                } else {
                    $fullurl .= '&amp;'.$querystring;
                }
            }

            if ($fullurl == $FULLME) {
                print_error(get_string('sharedspacehlooperror', 'sharedspaceh'));
            }
        } else {

            // Normal uploaded file.
            $forcedownloadsep = '?';
            if (isset($resource->options) && $resource->options == 'forcedownload') {
                $querys['forcedownload'] = '1';
            }
            $fullurl = sharedspaceh_get_file_url($this, $sharedspacehentry, $querys);
        }

        // Check whether this is supposed to be a popup, but was called directly.
        if (isset($resource->popup) && $resource->popup and !$inpopup) {
            // Make a page and a pop-up window.
            $coursecontext = \context_course::instance($course->id);
            $url = new moodle_url('/mod/sharedspaceh/view.php');
            $PAGE->set_url($url);
            $PAGE->set_pagelayout('popup');
            $PAGE->set_title($pagetitle);
            $PAGE->set_heading($SITE->fullname);
            $PAGE->navbar->add($course->fullname,'view.php','misc');

            $PAGE->set_focuscontrol('');
            $PAGE->set_cacheable(false);
            $PAGE->set_button('');

            $viewdata = array();
            $viewdata['popupoptions'] = $resource->popup;
            $viewdata['cmid'] = $cm->id;
            $PAGE->requires->js_call_amd('mod_sharedspaceh/view', 'init', $data);

            echo $OUTPUT->header();

            $template = new StdClass;
            $template->resid = $resource->id;
            if (trim(strip_tags($resource->intro))) {
                $template->infobox = $OUTPUT->box(format_text($resource->intro, $resource->introformat, $formatoptions), "center");
            }
            $template->linkurl = new moodle_url('/mod/sharedspaceh/view.php', array('inpopup' => true, 'id' => $cm->id));
            $template->popupoptions = $resource->popup;

            $template->title = format_string($resource->title, true);
            $template->strpopupresource = get_string('popupresource', 'resource');
            $template->strpopupresourcelink = get_string('popupresourcelink', 'resource', $template->linkurl);

            echo $OUTPUT->render_from_template('mod_sharedspaceh/directpopup', $template);

            echo $OUTPUT->footer($course);
            die;
        }

        // Now check whether we need to display a frameset.
        $frameset = optional_param('frameset', '', PARAM_ALPHA);
        if (empty($frameset) &&
                !$embedded &&
                        !$inpopup &&
                                (isset($resource->options) &&
                                        $resource->options == "frame") &&
                                                empty($USER->screenreader)) {
            @header('Content-Type: text/html; charset=utf-8');
            echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Frameset//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd\">\n";
            echo '<html dir="ltr">'."\n";
            echo '<head>';
            echo '<meta http-equiv="content-type" content="text/html; charset=utf-8" />';
            echo '<title>' . format_string($course->shortname) . ': '.strip_tags(format_string($resource->title,true)).'</title></head>'."\n";
            echo '<frameset rows="'.$config->framesize.',*">';
            $topurl = new moodle_url('/mod/sharedspaceh/view.php', array('id' => $cm->id, 'type' => $resource->type, 'frameset' => 'top'));
            echo '<frame src="'.$topurl.'" title="'. get_string('modulename','resource').'"/>';
            echo '<frame src="'.$fullurl.'" title="'.get_string('modulename','sharedspaceh').'"/>';
            echo '</frameset>';
            echo '</html>';
            exit;
        }

        // If we are in a frameset, just print the top of it
        if (!empty( $frameset ) and ($frameset == 'top') ) {
            $navigation = build_navigation($this->navlinks, $cm);
            $PAGE->set_pagelayout('frametop');
            $PAGE->set_context($system_context);
            $PAGE->set_title($pagetitle);
            $PAGE->set_heading($SITE->fullname);
            /* SCANMSG: may be additional work required for $navigation variable */
            $PAGE->navbar->add($strtitle,'view.php','misc');
            $PAGE->set_focuscontrol('');
            $PAGE->set_cacheable(false);
            $PAGE->set_button(update_module_button($cm->id, $course->id, $this->strresource));
            $PAGE->set_headingmenu(navmenu($course, $cm, 'parent'));
            $url = new moodle_url('/mod/sharedspaceh/view.php');
            $PAGE->set_url($url);

            echo $OUTPUT->header();

            $options = new stdClass();
            $options->para = false;
            echo '<div class="summary">'.format_text($resource->intro, $resource->introformat, $options).'</div>';

            echo $OUTPUT->footer('empty');
            exit;
        }

        // Display the actual resource.
        if ($embedded) {
            // Display resource embedded in page.
            $strdirectlink = get_string('directlink', 'sharedspaceh');
            $coursecontext = context_course::instance($course->id);

            if ($inpopup) {
                $PAGE->set_pagelayout('embedded');
            } else {
                $PAGE->set_pagelayout('popup');
            }

            $url = new moodle_url('/mod/sharedspaceh/view.php');
            $PAGE->set_url($url);

            $PAGE->set_title($pagetitle);
            $PAGE->set_heading($SITE->fullname);
            $PAGE->navbar->add($pagetitle,'view.php','misc');
            $PAGE->set_focuscontrol('');
            $PAGE->set_cacheable(false);
            $PAGE->set_button('');

            echo $OUTPUT->header();

            if ($resourcetype == 'image') {
                echo '<div class="resourcecontent resourceimg">';
                echo "<img title=\"".strip_tags(format_string($resource->title,true))."\" class=\"resourceimage\" src=\"$fullurl\" alt=\"\" />";
                echo '</div>';
            } else if ($resourcetype == 'mp3') {
                if (!empty($THEME->resource_mp3player_colors)) {
                    // You can set this up in your theme/xxx/config.php
                    $c = $THEME->resource_mp3player_colors;
                } else {
                    $c = 'bgColour=000000&btnColour=ffffff&btnBorderColour=cccccc&iconColour=000000&'.
                         'iconOverColour=00cc00&trackColour=cccccc&handleColour=ffffff&loaderColour=ffffff&'.
                         'font=Arial&fontColour=FF33FF&buffer=10&waitForPlay=no&autoPlay=yes';
                }
                $c .= '&volText='.get_string('vol', 'sharedspaceh').'&panText='.get_string('pan','sharedspaceh');
                $c = htmlentities($c);
                $id = 'filter_mp3_'.time(); // We need something unique because it might be stored in text cache.
                $cleanurl = ($fullurl);
                // If we have Javascript, use UFO to embed the MP3 player, otherwise depend on plugins.
                echo '<div class="resourcecontent resourcemp3">';
                echo '<span class="mediaplugin mediaplugin_mp3" id="'.$id.'"></span>'.
                     '<script type="text/javascript">'."\n".
                     '//<![CDATA['."\n".
                       'var FO = { movie:"'.$CFG->wwwroot.'/lib/mp3player/mp3player.swf?src='.$cleanurl.'",'."\n".
                         'width:"600", height:"70", majorversion:"6", build:"40", flashvars:"'.$c.'", quality: "high" };'."\n".
                       'UFO.create(FO, "'.$id.'");'."\n".
                     '//]]>'."\n".
                     '</script>'."\n";
                echo '<noscript>';
                echo "<object type=\"audio/mpeg\" data=\"$fullurl\" width=\"600\" height=\"70\">";
                echo "<param name=\"src\" value=\"$fullurl\" />";
                echo '<param name="quality" value="high" />';
                echo '<param name="autoplay" value="true" />';
                echo '<param name="autostart" value="true" />';
                echo '</object>';
                echo '<p><a href="' . $fullurl . '">' . $fullurl . '</a></p>';
                echo '</noscript>';
                echo '</div>';
            } else if ($resourcetype == 'flv') {
                $id = 'filter_flv_'.time(); // We need something unique because it might be stored in text cache.
                $cleanurl = ($fullurl);
                // If we have Javascript, use UFO to embed the FLV player, otherwise depend on plugins.
                echo '<div class="resourcecontent resourceflv">';
                echo '<span class="mediaplugin mediaplugin_flv" id="'.$id.'"></span>'.
                     '<script type="text/javascript">'."\n".
                     '//<![CDATA['."\n".
                       'var FO = { movie:"'.$CFG->wwwroot.'/filter/mediaplugin/flvplayer.swf?file='.$cleanurl.'",'."\n".
                         'width:"600", height:"400", majorversion:"6", build:"40", allowscriptaccess:"never", quality: "high" };'."\n".
                       'UFO.create(FO, "'.$id.'");'."\n".
                     '//]]>'."\n".
                     '</script>'."\n";
                echo '<noscript>';
                echo "<object type=\"video/x-flv\" data=\"$fullurl\" width=\"600\" height=\"400\">";
                echo "<param name=\"src\" value=\"$fullurl\" />";
                echo '<param name="quality" value="high" />';
                echo '<param name="autoplay" value="true" />';
                echo '<param name="autostart" value="true" />';
                echo '</object>';
                echo '<p><a href="' . $fullurl . '">' . $fullurl . '</a></p>';
                echo '</noscript>';
                echo '</div>';
            } else if ($resourcetype == 'mediaplayer') {
                echo '<div class="resourcecontent resourcewmv">';
                echo '<object type="video/x-ms-wmv" data="' . $fullurl . '">';
                echo '<param name="controller" value="true" />';
                echo '<param name="autostart" value="true" />';
                echo "<param name=\"src\" value=\"$fullurl\" />";
                echo '<param name="scale" value="noScale" />';
                echo "<a href=\"$fullurl\">$fullurl</a>";
                echo '</object>';
                echo '</div>';
            } else if ($resourcetype == 'mpeg') {
                echo '<div class="resourcecontent resourcempeg">';
                echo '<object classid="CLSID:22d6f312-b0f6-11d0-94ab-0080c74c7e95"
                              codebase="http://activex.microsoft.com/activex/controls/mplayer/en/nsm p2inf.cab#Version=5,1,52,701"
                              type="application/x-oleobject">';
                echo "<param name=\"fileName\" value=\"$fullurl\" />";
                echo '<param name="autoStart" value="true" />';
                echo '<param name="animationatStart" value="true" />';
                echo '<param name="transparentatStart" value="true" />';
                echo '<param name="showControls" value="true" />';
                echo '<param name="Volume" value="-450" />';
                echo '<!--[if !IE]>-->';
                echo '<object type="video/mpeg" data="' . $fullurl . '">';
                echo '<param name="controller" value="true" />';
                echo '<param name="autostart" value="true" />';
                echo "<param name=\"src\" value=\"$fullurl\" />";
                echo "<a href=\"$fullurl\">$fullurl</a>";
                echo '<!--<![endif]-->';
                echo '<a href="' . $fullurl . '">' . $fullurl . '</a>';
                echo '<!--[if !IE]>-->';
                echo '</object>';
                echo '<!--<![endif]-->';
                echo '</object>';
                echo '</div>';
            } else if ($resourcetype == 'rm') {
                echo '<div class="resourcecontent resourcerm">'; 
                echo '<object classid="clsid:CFCDAA03-8BE4-11cf-B84B-0020AFBBCCFA" width="320" height="240">';
                echo '<param name="src" value="' . $fullurl . '" />';
                echo '<param name="controls" value="All" />';
                echo '<!--[if !IE]>-->';
                echo '<object type="audio/x-pn-realaudio-plugin" data="' . $fullurl . '" width="320" height="240">';
                echo '<param name="controls" value="All" />';
                echo '<a href="' . $fullurl . '">' . $fullurl .'</a>';
                echo '</object>';
                echo '<!--<![endif]-->';
                echo '</object>';
                echo '</div>'; 
            } else if ($resourcetype == 'quicktime') {
                echo '<div class="resourcecontent resourceqt">';
                echo '<object classid="clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B"';
                echo '        codebase="http://www.apple.com/qtactivex/qtplugin.cab">';
                echo "<param name=\"src\" value=\"$fullurl\" />";
                echo '<param name="autoplay" value="true" />';
                echo '<param name="loop" value="true" />';
                echo '<param name="controller" value="true" />';
                echo '<param name="scale" value="aspect" />';
                echo '<!--[if !IE]>-->';
                echo "<object type=\"video/quicktime\" data=\"$fullurl\">";
                echo '<param name="controller" value="true" />';
                echo '<param name="autoplay" value="true" />';
                echo '<param name="loop" value="true" />';
                echo '<param name="scale" value="aspect" />';
                echo '<!--<![endif]-->';
                echo '<a href="' . $fullurl . '">' . $fullurl . '</a>';
                echo '<!--[if !IE]>-->';
                echo '</object>';
                echo '<!--<![endif]-->';
                echo '</object>';
                echo '</div>';
            }  else if ($resourcetype == 'flash') {
                echo '<div class="resourcecontent resourceswf">';
                echo '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000">';
                echo "<param name=\"movie\" value=\"$fullurl\" />";
                echo '<param name="autoplay" value="true" />';
                echo '<param name="loop" value="true" />';
                echo '<param name="controller" value="true" />';
                echo '<param name="scale" value="aspect" />';
                echo '<!--[if !IE]>-->';
                echo "<object type=\"application/x-shockwave-flash\" data=\"$fullurl\">";
                echo '<param name="controller" value="true" />';
                echo '<param name="autoplay" value="true" />';
                echo '<param name="loop" value="true" />';
                echo '<param name="scale" value="aspect" />';
                echo '<!--<![endif]-->';
                echo '<a href="' . $fullurl . '">' . $fullurl . '</a>';
                echo '<!--[if !IE]>-->';
                echo '</object>';
                echo '<!--<![endif]-->';
                echo '</object>';
                echo '</div>';
            } elseif ($resourcetype == 'zip') {
                echo '<div class="resourcepdf">';
                echo get_string('clicktoopen', 'resource') . '<a href="' . $fullurl . '">' . format_string($resource->title) . '</a>';
                echo '</div>';
            } elseif ($resourcetype == 'pdf') {
                echo '<div class="resourcepdf">';
                echo '<object data="' . $fullurl . '" type="application/pdf">';
                echo get_string('clicktoopen', 'resource') . '<a href="' . $fullurl . '">' . format_string($resource->title) . '</a>';
                echo '</object>';
                echo '</div>';
            }
            if (trim($resource->intro)) {
                echo $OUTPUT->box(format_text($resource->intro, $resource->introformat, $formatoptions, $course->id), "center");
            }
            if ($inpopup) {
                // Suppress the banner that gets cutoff with large images.
                echo '<style> body.HAT-narrowbg {background:none};</style>';
                echo "<div class=\"popupnotice\">(<a href=\"$fullurl\">$strdirectlink</a>)</div>";
                echo "</div>"; // MDL-12098
            } else {
                print_spacer(20, 20);
            }
        } else {
            // Display the resource on it's own.
            redirect($fullurl);
        }
    }

    /**
     * Finish displaying the sharedspaceh with the course blocks
     * Given an object containing all the necessary data,
     * (defined by the form in mod.html) this function
     * will create a new instance and return the id number
     * of the new instance.
     *
     * @param sharedspaceh   object, sharedspaceh record values
     * @return int, sharedspaceh id or false      
     */
    public function add_instance() {
        global $DB;

        $this->_postprocess();
        $this->sharedspaceh->timemodified = time();
        return $DB->insert_record('sharedspaceh', $this->sharedspaceh);
    }

    /**
     * Given an object containing all the necessary data,
     * (defined by the form in mod.html) this function
     * will update an existing instance with new data.
     *
     * @param sharedspaceh   object, sharedspaceh record values
     * @return bool sharedspaceh insert status      
     */
    public function update_instance() {
        global $DB;

        $this->_postprocess();
        $this->sharedspaceh->id = $this->sharedspaceh->instance;
        $this->sharedspaceh->timemodified = time();
        return $DB->update_record('sharedspaceh', $this->sharedspaceh);
    }

    /**
     * Given an object containing the sharedspaceh data
     * this function will permanently delete the instance
     * and any data that depends on it.
     *
     * @param sharedspaceh   object, sharedspaceh record values
     * @return bool sharedspaceh delete status
     */
    public function delete_instance() {
        global $DB;

        $result = true;
        if (!$DB->delete_records('sharedspaceh', array('id' => $this->sharedspaceh->id))) {
            $result = false;
        }
        return $result;
    }

    /**
     * Sets the parameters property of the extended class
     *
     * @uses $USER  global object
     * @uses $CFG   global object
     */
    public function set_parameters() {
        global $USER, $CFG, $PAGE, $OUTPUT, $SITE, $DB;

        $site = get_site();
        $littlecfg = new stdClass();
        $littlecfg->wwwroot = $CFG->wwwroot;
        
        $course = $DB->get_record('course', array('id' => $this->sharedspaceh->course));

        $this->parameters = array(
                'label2'          => array('langstr' => "",
                                           'value'   =>'/optgroup'),
                'label3'          => array('langstr' => get_string('course'),
                                           'value'   => 'optgroup'),
                'courseid'        => array('langstr' => 'id',
                                           'value'   => $this->sharedspaceh->course),
                'coursefullname'  => array('langstr' => get_string('fullname'),
                                           'value'   => $course->fullname),
                'courseshortname' => array('langstr' => get_string('shortname'),
                                           'value'   => $course->shortname),
                'courseidnumber'  => array('langstr' => get_string('idnumbercourse'),
                                           'value'   => $course->idnumber),
                'coursesummary'   => array('langstr' => get_string('summary'),
                                           'value'   => $course->summary),
                'courseformat'    => array('langstr' => get_string('format'),
                                           'value'   => $course->format),
                'label4'          => array('langstr' => '',
                                           'value'   =>'/optgroup'),
                'label5'          => array('langstr' => get_string('miscellaneous'),
                                           'value'   => 'optgroup'),
                'lang'            => array('langstr' => get_string('preferredlanguage'),
                                           'value'   => current_language()),
                'sitename'        => array('langstr' => get_string('fullsitename'),
                                           'value'   => format_string($site->fullname)),
                'serverurl'       => array('langstr' => get_string('serverurl', 'sharedspaceh', $littlecfg),
                                           'value'   => $littlecfg->wwwroot),
                'currenttime'     => array('langstr' => get_string('time'),
                                           'value'   => time()),
                'label6'          => array('langstr' => "",
                                           'value'   =>'/optgroup')
        );

        if (!empty($USER->id)) {
            $userparameters = array(
                'label1'          => array('langstr' => get_string('user'),
                                           'value'   => 'optgroup'),
                'userid'          => array('langstr' => 'id',
                                           'value'   => $USER->id),
                'userusername'    => array('langstr' => get_string('username'),
                                           'value'   => $USER->username),
                'useridnumber'    => array('langstr' => get_string('idnumber'),
                                           'value'   => $USER->idnumber),
                'userfirstname'   => array('langstr' => get_string('firstname'),
                                           'value'   => $USER->firstname),
                'userlastname'    => array('langstr' => get_string('lastname'),
                                           'value'   => $USER->lastname),
                'userfullname'    => array('langstr' => get_string('fullname'),
                                           'value'   => fullname($USER)),
                'useremail'       => array('langstr' => get_string('email'),
                                           'value'   => $USER->email),
                'usericq'         => array('langstr' => get_string('icqnumber'),
                                           'value'   => $USER->icq),
                'userphone1'      => array('langstr' => get_string('phone').' 1',
                                           'value'   => $USER->phone1),
                'userphone2'      => array('langstr' => get_string('phone2').' 2',
                                           'value'   => $USER->phone2),
                'userinstitution' => array('langstr' => get_string('institution'),
                                           'value'   => $USER->institution),
                'userdepartment'  => array('langstr' => get_string('department'),
                                           'value'   => $USER->department),
                'useraddress'     => array('langstr' => get_string('address'),
                                           'value'   => $USER->address),
                'usercity'        => array('langstr' => get_string('city'),
                                           'value'   => $USER->city),
                'usertimezone'    => array('langstr' => get_string('timezone'),
                                           'value'   => 0),
                'userurl'         => array('langstr' => get_string('webpage'),
                                           'value'   => $USER->url)
             );
             $this->parameters = $userparameters + $this->parameters;
        }
    }
    
    /**
     * set up form elements for add/update of sharedspaceh
     *
     * @param mform   object, reference to Moodle Forms object
     */
    public function setup_elements(&$mform) {
        global $CFG, $USER, $METASHAREDRC_WINDOW_OPTIONS, $DB, $OUTPUT;

        $config = get_config('sharedspaceh');

        $add     = optional_param('add', 0, PARAM_ALPHA);
        $update  = optional_param('update', 0, PARAM_INT);
        $return  = optional_param('return', 0, PARAM_BOOL); // Return to course/view.php if false or mod/modname/view.php if true.
        $type    = optional_param('type', 'file', PARAM_ALPHANUM);
        $section = optional_param('section', null, PARAM_INT);
        $courseid  = optional_param('course', null,PARAM_INT);

        if (!empty($add)) {
            // We may just have created one.
            $entryid = optional_param('entryid', false, PARAM_INT);
            // Have we selected a resource yet ?
            if (empty($entryid)) {
                $params = array('course' => $courseid,
                                'section' => $section,
                                'type' => $type,
                                'add' => $add,
                                'return' => $return,
                                'entryid' => $entryid,
                                'sesskey' => sesskey());
                redirect(new moodle_url('/mod/sharedspaceh/search.php', $params));
            } else {
                // We have our reference Shared resource.
                if (!$shrentry = \mod_sharedspaceh\entry::read_by_id($entryid)) {
                    print_error('errorinvalididentifier', 'sharedspaceh', $entryid);
                }
            }
        } else if (!empty($update)) {
            if (! $cm = get_coursemodule_from_id('sharedspaceh', $update)) {
                print_error('invalidcoursemodule');
            }
            if (! $resource = $DB->get_record('sharedspaceh', array('id' => $cm->instance))) {
                print_error('errorinvalidresource', 'sharedspaceh');
            }
            if (!$shrentry = \mod_sharedspaceh\entry::read($resource->identifier)) {
                print_error('errorinvalididentifier', 'sharedspaceh', $resource->identifier);
            }
        }
        // Set the parameter array for the form.
        $this->set_parameters();
        
        if(isset($shrentry)) {
            $mform->addElement('hidden', 'entryid', $shrentry->id);
            $mform->setType('entryid', PARAM_INT);

            $mform->addElement('hidden', 'identifier', $shrentry->identifier);
            $mform->setType('identifier', PARAM_TEXT);

            $mform->setDefault('name', $shrentry->title);
            $mform->setDefault('description', ($shrentry->description));

            $location = $mform->addElement('static', 'origtitle', get_string('title', 'sharedspaceh').': ', ($shrentry->title));
        
            $strpreview = get_string('preview','sharedspaceh');
            
            if (empty($config->foreignurl)) {
                $params = array('identifier' => $shrentry->identifier, 'inpopup' => true);
                $resurl = new moodle_url('/mod/sharedspaceh/view.php', $params);
                $link =  '<a href="'.$resurl.'" '
                  . "onclick=\"this.target='resource{$shrentry->id}'; return openpopup('".$resurl."', "
                  . "'resource{$shrentry->id}','resizable=1,scrollbars=1,directories=1,location=0,menubar=0,toolbar=0,status=1,width=800,height=600');\">(".$strpreview.")</a>";
            } else {
                $url = str_replace('<%%ID%%>', $shrentry->identifier, $config->foreignurl);
                $link = '<a href="'.$url.'" target="_blank">('.$strpreview.')</a>';
            }
        }
        
        if(isset($link)) {
            $location = $mform->addElement('static', 'url', get_string('location', 'sharedspaceh').': ', $link);
        }
        
        $searchbutton = $mform->addElement('submit', 'searchsharedspaceh', get_string('searchsharedspaceh', 'sharedspaceh'));
        $params = array('course' => $this->sharedspaceh->course,
                        'section' => $section,
                        'type' => $type,
                        'add' => $add,
                        'return' => $return);
        $searchurl = new moodle_url('/mod/sharedspaceh/search.php', $params);
        $buttonattributes = array('title' => get_string('searchsharedspaceh', 'sharedspaceh'),
                                  'onclick' => " window.location.href ='".$searchurl."'; return false;");
        $searchbutton->updateAttributes($buttonattributes);

        $mform->addElement('header', 'displaysettings', get_string('display', 'sharedspaceh'));

        $mform->addElement('checkbox', 'forcedownload', get_string('forcedownload', 'sharedspaceh'));
        $mform->disabledIf('forcedownload', 'windowpopup', 'eq', 1);

        $woptions = array(0 => get_string('pagewindow', 'sharedspaceh'), 1 => get_string('newwindow', 'sharedspaceh'));
        $mform->addElement('select', 'windowpopup', get_string('display', 'sharedspaceh'), $woptions);
        $mform->setType('windowpopup', PARAM_INT);
        $mform->setDefault('windowpopup', (empty($config->popup) ? 1 : 0));
        $mform->disabledIf('windowpopup', 'forcedownload', 'checked');

        $mform->addElement('checkbox', 'framepage', get_string('keepnavigationvisible', 'sharedspaceh'));
        $mform->addHelpButton('framepage', 'frameifpossible', 'sharedspaceh');
        $mform->setDefault('framepage', 0);
        $mform->disabledIf('framepage', 'windowpopup', 'eq', 1);
        $mform->disabledIf('framepage', 'forcedownload', 'checked');
        $mform->setAdvanced('framepage');

        foreach ($METASHAREDRC_WINDOW_OPTIONS as $option) {
            if ($option == 'height' or $option == 'width') {
                $mform->addElement('text', $option, get_string('new'.$option, 'sharedspaceh'), array('size'=>'4'));
                $mform->setType($option, PARAM_TEXT);
              //  $mform->setDefault($option, $config->{'popup'.$option});
                $mform->disabledIf($option, 'windowpopup', 'eq', 0);
            } else {
                $mform->addElement('checkbox', $option, get_string('new'.$option, 'sharedspaceh'));
//                $mform->setDefault($option, $config->{'popup'.$option});
                $mform->setType($option, PARAM_INT); 
                $mform->disabledIf($option, 'windowpopup', 'eq', 0);
            }
            $mform->setAdvanced($option);
        }
        $mform->addElement('header', 'parameters', get_string('parameters', 'sharedspaceh'));

        $options = array();
        $options['-'] = get_string('chooseparameter', 'sharedspaceh').'...';
        $optgroup = '';
        foreach ($this->parameters as $pname => $param) {
            if ($param['value']=='/optgroup') {
                $optgroup = '';
                continue;
            }
            if ($param['value'] == 'optgroup') {
                $optgroup = $param['langstr'];
                continue;
            }
            $options[$pname] = $optgroup.' - '.$param['langstr'];
        }
        for ($i = 0; $i < $this->maxparameters; $i++) {
            $parametername = "parameter$i";
            $parsename = "parse$i";
            $group = array();
            $group[] =& $mform->createElement('text', $parsename, '', array('size'=>'12')); // TODO: accessiblity.
            $group[] =& $mform->createElement('select', $parametername, '', $options); // TODO: accessiblity.
            $label = get_string('variablename', 'sharedspaceh').'='.get_string('parameter', 'sharedspaceh');
            $mform->addGroup($group, 'pargroup'.$i, $label, ' ', false);
            $mform->setAdvanced('pargroup'.$i);
            $mform->setType($parsename, PARAM_RAW);
            $mform->setDefault($parametername, '-');
        }
    }

    /**
     * set up form element default values prior to display for add/update of sharedspaceh
     *
     * @param default_values   object, reference to form default values object
     */
    public function setup_preprocessing(&$default_values) {
        // Override to add your own options.
        assert(1);
    }
}
