<?php
require_once '../../config.php';

global $CFG, $DB;
require_once $CFG->dirroot.'/blocks/sharing_cart/classes/controller.php';

try {
    $controller = new sharing_cart\controller();
    
    $cmid     = required_param('cmid', PARAM_INT);
    $userdata = 0;
    $course   = required_param('course', PARAM_INT);         
    $sectionnumber = required_param('section', PARAM_INT);    
    
    $controller->backup($cmid, $userdata, $course);
    
    
    if ($course == SITEID) {
        $returnurl = new moodle_url('/');
    } else {
        $returnurl = new moodle_url('/course/view.php', array('id' => $course));
    }

    $returnurl .= '#section-' . $sectionnumber;
    
    $id = array_keys($DB->get_records_sql("SELECT id FROM {block_sharing_cart} order by id DESC LIMIT 1"))[0];
    $controller->restore($id, $course, $sectionnumber);
    $controller->delete($id);
    
    
    redirect($returnurl);

} catch (Exception $ex) {
    echo $ex->getMessage();
    die();
}
