<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function sharedspaceh_dndupload_register() {
        $ret = array();
    
    $strdndtext = get_string('dnduploadlabeltext', 'mod_label');
    return array_merge($ret, array('types' => array(
        array('identifier' => 'text/html', 'message' => "super message", 'noname' => true),
        array('identifier' => 'text', 'message' => "Super message", 'noname' => true)
    )));
}

function sharedspaceh_add_instance($sharedspaceh) {
    global $DB;
    return $DB->insert_record('sharedspaceh', $sharedspaceh);
}

function sharedspaceh_delete_instance($id) {
    global $DB;

    if (!$metasharedrc = $DB->get_record('sharedspaceh', array('id' => $id))) {
        return false;
    }

    $result = true;
    if (!$DB->delete_records('sharedspaceh', array('id' => $id))) {
        $result = false;
    }
    return $result;
}

function sharedspaceh_cm_info_dynamic(&$modinfo) {
    global $DB, $USER, $CFG, $COURSE;

    require_once($CFG->libdir.'/filelib.php');
    $context = context_course::instance($COURSE->id);
    
    if ($sharedspaceh = $DB->get_record('sharedspaceh', array('id' => $modinfo->instance))) {

        if (strpos($sharedspaceh->url, '?id=') !== false) {
            
            if ($course_modules = $DB->get_record('course_modules', array('id' => substr($sharedspaceh->url,strpos($sharedspaceh->url, '?id=')+4)), 'module')) {
                
                $name = $DB->get_record('modules', array('id' => $course_modules->module), 'name');
                
                if (!empty($name->name)) {
                    if (user_has_role_assignment($USER->id, 5, $context->id) && !user_has_role_assignment($USER->id, 1, $context->id)) {
                        $modinfo->set_icon_url(new moodle_url($CFG->wwwroot.'/mod/'.$name->name."/pix/icon.png"));
                    } elseif((!user_has_role_assignment($USER->id, 5, $context->id) && !user_has_role_assignment($USER->id,1, $context->id)) && !user_has_role_assignment($USER->id,1)) {
                         $modinfo->set_icon_url(new moodle_url($CFG->wwwroot.'/mod/'.$name->name."/pix/icon.png"));
                    } else {
                        $modinfo->set_icon_url(new moodle_url($CFG->wwwroot.'/mod/'.$name->name."/pix/icon.png"));
                        $modinfo->set_name("[SHARED] ".trim($sharedspaceh->name,"[SHARED] "));
                    }
                }
            }
        }
    }
    /*
    if (!$shrentry) {
        $modinfo->set_icon_url($OUTPUT->image_url('broken', 'metasharedrc'));
    } else {
        if (user_has_role_assignment($USER->id, 5, $context->id) && !user_has_role_assignment($USER->id, 1, $context->id)) {
            $modinfo->set_icon_url(new moodle_url($CFG->wwwroot.'/mod/'.$shrentry->mname."/pix/icon.png"));
        } elseif((!user_has_role_assignment($USER->id, 5, $context->id) && !user_has_role_assignment($USER->id,1, $context->id)) && !user_has_role_assignment($USER->id,1)) {
             $modinfo->set_icon_url(new moodle_url($CFG->wwwroot.'/mod/'.$shrentry->mname."/pix/icon.png"));
        } 
    }*/
}