<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

global $CFG,$PAGE,$DB;

require('../../config.php');
require_once($CFG->libdir.'/adminlib.php');
require_once($CFG->libdir.'/outputrenderers.php');
require_once($CFG->libdir.'/tablelib.php');
//require_once('lib.php');
require_once($CFG->dirroot.'/local/sharedspaceh/classes/space_form.php');
require_once($CFG->dirroot.'/local/sharedspaceh/classes/space_table.php');
require_once($CFG->dirroot.'/local/sharedspaceh/spacelib.php');
require_once($CFG->dirroot.'/local/sharedspaceh/classes/space_form_research.php');
require_once($CFG->dirroot.'/local/sharedspaceh/classes/sharedspaceh_form_quicksearch_edit.php');
require_once($CFG->dirroot.'/local/sharedspaceh/lib.php');
require_once($CFG->dirroot.'/local/sharedspaceh/classes/sharedspaceh_form_columns_edit.php');

$strtitle = get_string('addinstance', 'sharedspaceh');
$PAGE->set_pagelayout('standard');

$add     = optional_param('add', 0, PARAM_ALPHA);
$update  = optional_param('update', 0, PARAM_INT);
$return  = optional_param('return', 0, PARAM_BOOL); // Return to course/view.php if false or mod/modname/view.php if true.
$type    = optional_param('type', 'file', PARAM_ALPHANUM);
$section = optional_param('section', null, PARAM_INT);
$courseid  = optional_param('course', null,PARAM_INT);
$sesskey = optional_param('sesskey', null,PARAM_INT);

$params = array('course' => $courseid,
                'section' => $section,
                'type' => $type,
                'add' => $add,
                'return' => $return,
           //     'entryid' => $entryid,
                'sesskey' => $sesskey);

$url = new moodle_url('/mod/sharedspaceh/search.php',$params);
$PAGE->set_url($url);

$systemcontext = context_system::instance();
$PAGE->set_context($systemcontext);
$PAGE->set_title($strtitle);
$PAGE->set_heading($SITE->fullname);

$PAGE->navbar->add(get_string('searchsharedspaceh', 'sharedspaceh'));

//Inclusion css et Javascript 
$PAGE->requires->css('/local/sharedspaceh/css/style.css');
$PAGE->requires->css('/mod/sharedspaceh/css/style.css');
$PAGE->requires->js_call_amd('local_sharedspaceh/form_manager', 'init');

//TEST EXTENDING MEMORTY
//$initialmem = ini_get('memory_limit');
//ini_set('memory_limit', get_real_size('512M'));

// Fake block Form
$space_form_research = new space_form_research();
$bc = new block_contents();
$bc->title = get_string('research', 'sharedspaceh').html_writer::start_span('advanced-sharedspaceh-title') . "[".get_string('advanced', 'sharedspaceh')."]" . html_writer::end_span().html_writer::start_span('edit_quicksearch-sharedspaceh') . "[Edit]" . html_writer::end_span();
$bc->attributes['id'] = 'shared_space_block_research';

$mform = new space_form();
    
if (isset($_POST['_qf__space_form'])) {
    $data_set = $mform->get_data()->elemarray;
    foreach ($data_set as $k => $ds) {
        if ($ds === "") {
            unset ($data_set[$k]);
        }
    }
    if (isset($mform->get_data()->limit_nb_results)) {
        $data_set['limit_nb_results'] = $mform->get_data()->limit_nb_results;//TODO
    } else {
        $data_set['limit_nb_results'] = 100;
    }

    foreach ($mform->get_data()->elemarray_adv as $key => $dss ) {
        if ($key != "cm_id" && $dss != "") {
            $data_set[$key] = $dss;
        }
    }
    $space_form_research->set_data($data_set);
}

$mform_advanced = new space_form();
    
if (isset($_POST['_qf__space_form_research'])) {
    $data = new stdClass;
    $date_set_advanced = (array)$space_form_research->get_data();
    foreach ($date_set_advanced as $k => $ds) {
        if ($ds === "") {
            unset ($date_set_advanced[$k]);
        }
    }
    $data->elemarray = $date_set_advanced;
    $data->elemarray_adv = $date_set_advanced;
    $mform->set_data($data);
    $mform_advanced->set_data($data);
}

$download      = optional_param('download', '', PARAM_ALPHA);
$table = new space_table(28101976);
$table->is_downloading($download, 'request','shared space request');

require_login();

if (isset ($space_form_research)) {
    
    if (isset($_GET['requestknown'])) {//TODO: check if data is in the url...

            if (sizeof($_POST) != 0) {
                $request         = new space_request();
            } else {
                $request         = new space_request(true);
            }
            if (isset($_GET['scopesearch']) &&  has_capability('local/sharedspaceh:accessasadmin',$systemcontext)) {
                $request->set_search_all($_GET['scopesearch']);
            } else {
                $request->set_search_all(false);
            }
            if (isset($_GET['limit_nb_results'])) {
                $request->set_field_name_and_id('Limit in number of results',  'limit_nb_results');
                $request->add_filter('limit_nb_results', $_GET['limit_nb_results']);
                //echo 'Limit in number of modules' . ' ' . 'limit_nb_results' . ' ' . $_GET['limit_nb_results'] .' </br>';
                //$request->show_request();
                //echo ' ' . $request->get_values_of_field_from_id('limit_nb_results') . ' </br>';
            }

            $fieldnames = $space_form_research->get_all_fields_headers();
            foreach ($fieldnames as $fname) {
                read_array_of_field_values($fname, $space_form_research->get_field_id($fname), $request);
            }
            $creteria_merge = $request->get_field_criteria();
            $creteria_merge["course_name"] = $request->get_values_of_field_from_id('course_name');
            $creteria_merge["cm_name"] = $request->get_values_of_field_from_id('cm_name');
            $creteria_merge["cm_id"] = $request->get_values_of_field_from_id('cm_id');
            $creteria_merge["free_search_keywords"] = $request->get_values_of_field_from_id('free_search_keywords');
            $creteria_merge["limit_nb_results"] = $request->get_values_of_field_from_id('limit_nb_results')[0];

            $space_form_research->set_data($creteria_merge);
            
            if (!isset($data)) {
                $data = new stdClass;
            }
            
            $data->elemarray = $creteria_merge;
            $data->elemarray_adv = $creteria_merge;
            $mform_advanced->set_data($data);
            $mform->set_data($data);
            
        }
        
        if (!isset($_GET['requestknown'])) {
            if ($data= $space_form_research->get_data()) { 
                $request = $space_form_research->get_space_request();
            } else if (!isset($_POST['_qf__space_form'])) {
                $request = new space_request(true);
            }/* else if (isset($_POST['_qf__space_form'])) {
                $request = $space_form_research->get_space_request();
            }*/
        }
}

if (!$table->is_downloading()) { 
    $PAGE->set_title(get_string('sharedspaceh', 'local_sharedspaceh'));
    $PAGE->set_blocks_editing_capability('moodle/my:manageblocks');

    $PAGE->blocks->add_region('content');
    $PAGE->blocks->load_blocks(true);
    $allblocks = $PAGE->blocks->get_blocks_for_region('side-pre');
    $shared_card_block = $allblocks[0];
    
    if ($shared_card_block) {
        $shared_card_block_content = $shared_card_block->get_content();
    }
    
    $is_cart_in = $PAGE->blocks->is_block_present('sharing_cart');
    if (!$is_cart_in) {
        $PAGE->blocks->add_block('sharing_cart','side-pre',1,true);
    } 
}

$bc->content = $space_form_research->render();
$PAGE->blocks->add_fake_block($bc, 'side-pre');

if (!$table->is_downloading()) {
    echo $OUTPUT->header();
    echo $OUTPUT->heading(get_string('addactivityincourse', 'sharedspaceh').$DB->get_record('course', array('id' => optional_param('course', 0, PARAM_INT)))->fullname);
    echo $OUTPUT->custom_block_region('content');
}


if ($mform->is_cancelled()) {
        //Handle form cancel operation, if cancel button is present on form
} else if ($data= $mform->get_data()) {
        $request = $mform->get_space_request();
        display($mform, $request, $table);
    } else if (isset ($space_form_research)) {
        display_research($space_form_research, $request, $table);
}

    
$PAGE->requires->js_call_amd('local_sharedspaceh/advanced_research', 'init',array($mform->get_all_id_field_available(), $CFG->wwwroot));
echo html_writer::start_div('advanced-research-mask-sharedspaceh', array()).html_writer::end_div();
    
//Display the advanced research form
echo    html_writer::start_div('advanced-research-sharedspaceh', array()).
        html_writer::start_span('title-advanced-research-sharedspaceh') . get_string('advancedreaserch', 'sharedspaceh') . html_writer::end_span().
        html_writer::start_span('title-advanced-research-close-sharedspaceh') . "[Close]" . html_writer::end_span().
        $mform_advanced->render().
        html_writer::end_div();
    
//Displayy the quick search editing form
$sharedspaceh_form_quicksearch_edit = new sharedspaceh_form_quicksearch_edit();
echo    html_writer::start_div('edit-quicksearch-box-sharedspaceh', array()).
        html_writer::start_span('title-edit-quicksearch-sharedspaceh') . get_string('customquicksearch', 'sharedspaceh') . html_writer::end_span().
        $sharedspaceh_form_quicksearch_edit->render().
        html_writer::end_div();

$sharedspaceh_form_columns_edit = new sharedspaceh_form_columns_edit();
        echo html_writer::start_div('edit-columns-box-sharedspaceh', array()).
                 html_writer::start_span('title-edit-quicksearch-sharedspaceh') . get_string('customeditcolumns', 'local_sharedspaceh') . html_writer::end_span().
            $sharedspaceh_form_columns_edit->render().
            html_writer::end_div();

echo $OUTPUT->footer();
//ini_set('memory_limit', $initial_mem);


/** 
 * - perform the request based on the criteria set in $data_criteria
 * - display the resulting table in the page
 * Display the result
 */
function display_research(space_form_research $mform, space_request $request, space_table &$table) {
    global $DB, $OUTPUT;
    $results = $request->run_request();//h_simple_request($data_criteria);      
    if (!$table->is_downloading()) {
        echo $OUTPUT->box_start();
    }
    if (!empty($results)) { //test
        /*
        if (!$table->is_downloading()) {
           echo $OUTPUT->heading('Result');
        }
         * */
         
        $table->init($mform, $results);
        $table->define_caseine_baseurl($request);
        $table->fill_caseine_table();
        echo "<br />".html_writer::span("Manage Columns","edit-columns-span");
        $table->finish_output();
    } else {
        if (!$table->is_downloading()) {
            echo '<p style="text-align: center">' . get_string('nocoursefound', 'local_sharedspaceh') . "</p>";
        }
    }
    if (!$table->is_downloading()) {
        echo $OUTPUT->box_end();
    }
    $table->hide_conf_columns();
}

/**
 * - perform the request based on the criteria set in $data_criteria
 * - display the resulting table in the page
 * Display the result
 */
function display(space_form $mform, space_request $request, space_table &$table) {
    global $DB, $OUTPUT;
    $results = $request->run_request();//h_simple_request($data_criteria);      
    if (!$table->is_downloading()) {
        echo $OUTPUT->box_start();
    }
    if (!empty($results)) { //test
     /*   if (!$table->is_downloading()) {
            echo $OUTPUT->heading('Result');
        }*/
        $table->init($mform, $results);
        $table->define_caseine_baseurl($request);
        $table->fill_caseine_table();
        $table->finish_output();
    } else {
        if (!$table->is_downloading()) {
            echo '<p style="text-align: center">' . get_string('nocoursefound', 'local_sharedspaceh') . "</p>";
        }
    }
    if (!$table->is_downloading()) {
        echo $OUTPUT->box_end();
    }
    $table->hide_conf_columns();
}

/**
 * Read the possible values of a meta-data from the url
 * and set properly the $request object used to perform the request.
 */
function read_array_of_field_values(string $fname, string $fid, space_request &$request) {
    $fnamebar = str_replace(' ','',$fname);
    $id = 0;
    while (isset($_GET[$fnamebar . $id])) {
        if ($id == 0) {
            $request->set_field_name_and_id($fname,  $fid);
        }
        $request->add_filter($fid, $_GET[$fnamebar . $id]);
        $id++; 
    }
}