<?php

require('../../config.php');

$id = optional_param('id', 0, PARAM_INT);    // Course Module ID.

if ($cm = $DB->get_record('course_modules', array('id' => $id))) {
    if ($sharedspaceh = $DB->get_record('sharedspaceh', array('id' => $cm->instance))) {
        if (!empty($sharedspaceh->url)) {
            redirect ($sharedspaceh->url);
        }
    }
}