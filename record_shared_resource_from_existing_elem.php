<?php
require_once(dirname(__FILE__) . '/../../config.php');

global $CFG, $DB, $PAGE;

require_once("lib.php");
require_once($CFG->libdir.'/filelib.php');
require_once($CFG->libdir.'/gradelib.php');
require_once($CFG->libdir.'/completionlib.php');
require_once($CFG->libdir.'/plagiarismlib.php');
require_once($CFG->dirroot . '/course/modlib.php');

   $course = $DB->get_record('course', array('id'=> $_GET['course'] ), '*', MUST_EXIST);
   list($module, $context, $cw, $cm, $data) = prepare_new_moduleinfo_data($course, "sharedspaceh", 0);
   
   $PAGE->set_context($context);
   
   
    $data->return = 0;
    $data->sr = 0;
    $data->add = "sharedspaceh";
    $data->type = "file";
    
$modmoodleform = "$CFG->dirroot/mod/sharedspaceh/mod_form.php";
if (file_exists($modmoodleform)) {
    require_once($modmoodleform);
} else {
    print_error('noformdesc');
}

$mformclassname = 'mod_sharedspaceh_mod_form';
$mform = new $mformclassname($data, $cw->section, $cm, $course);
$mform->set_data($data);


  
 
  
 $object = new stdClass;
 $object->windowpopup = "1";
 $object->width = "800";
 $object->visibleoncoursepage = 1;
 $object->visible = 1;
 $object->toolbar = 1;
  $object->status = 1;
 $object->showdescription = "0";     
 $object->srollbars = 1;
 $object->resizable = 1;
 $object->popupresizable=1;
 //**
 $object->name = $_GET['cm_name'];
 $introeditor["text"] = "<p>Description <br /></p>";
 $introeditor["itemid"] = "220969228";
 $introeditor["format"] = "1";
 $object->introeditor = $introeditor;
 

 $object->modulename ="sharedspaceh";
 $object->module = $DB->get_record('modules', array('name'=>"sharedspaceh"))->id;
 $object->section = $_GET['section'];
 $object->url = $CFG->wwwroot."/mod/".$_GET['cm_modname']."/view.php?id=".$_GET['cm_id'];
 $object->timemodified = time();

 //$fromform = $mform->get_data();
 $fromform = $object;
 add_moduleinfo($fromform, $course, $mform);
 
 redirect($CFG->wwwroot."/course/view.php?id=".$_GET['course']."&section=".$_GET['section']."&add=sharedspaceh");
